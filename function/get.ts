import * as fs from "fs";

module.exports = (req: any, res: any) => {
  fs.readFile("users.json", "utf8", (err, data) => {
    if (err) {
      console.error(err);
      res.statusCode = 500;
      res.end("Error reading file");
    } else {
      const users = data ? JSON.parse(data) : [];
      res.setHeader("Content-Type", "application/json");
      res.statusCode = 200;
      res.end(JSON.stringify(users));
    }
  });
};
