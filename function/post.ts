import * as fs from "fs";

module.exports = (req: any, res: any) => {
  let body = "";
  req.on("data", (chunk: any) => {
    body += chunk;
  });
  req.on("end", () => {
    const userData: any = JSON.parse(body);
    fs.readFile("users.json", "utf8", (err, data) => {
      if (err) {
        console.error(err);
        res.statusCode = 500;
        res.end("Error reading file");
      } else {
        const users = data ? JSON.parse(data) : [];
        users.push(userData);
        fs.writeFile("users.json", JSON.stringify(users), (err) => {
          if (err) {
            res.statusCode = 500;
            res.end("Error writing file");
          } else {
            res.statusCode = 200;
            res.end("User saved successfully");
          }
        });
      }
    });
  });
};
