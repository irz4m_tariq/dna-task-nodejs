import * as http from "http";

const getReq = require("./function/get");
const postReq = require("./function/post");

const server = http.createServer((req, res) => {
  if (req.method === "POST" && req.url === "/users") {
    postReq(req, res);
  } else if (req.method === "GET" && req.url === "/users") {
    getReq(req, res);
  } else {
    res.statusCode = 404;
    res.setHeader("Content-Type", "application/json");
    res.write(JSON.stringify({ title: "error", message: "not found" }));
    res.end();
  }
});

server.listen(3000, () => {
  console.log("Server running at http://localhost:3000");
});
